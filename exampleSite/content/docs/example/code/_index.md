---
title: Code
createdDate: 2023-11-04
bookSection: false
---

This page templates out some example code fragments to test that they render correctly.


<!--more-->

## Python


```python
# Program to display the Fibonacci sequence up to n-th term

nterms = int(input("How many terms? "))

# first two terms
n1, n2 = 0, 1
count = 0

# check if the number of terms is valid
if nterms <= 0:
   print("Please enter a positive integer")
# if there is only one term, return n1
elif nterms == 1:
   print("Fibonacci sequence upto",nterms,":")
   print(n1)
# generate fibonacci sequence
else:
   print("Fibonacci sequence:")
   while count < nterms:
       print(n1)
       nth = n1 + n2
       # update values
       n1 = n2
       n2 = nth
       count += 1

```

## Java

```Java
class FibonacciExample1{
  public static void main(String args[])
  {
    int n1=0,n2=1,n3,i,count=10;
    System.out.print(n1+" "+n2);//printing 0 and 1

    for(i=2;i<count;++i)//loop starts from 2 because 0 and 1 are already printed
    {
      n3=n1+n2;
      System.out.print(" "+n3);
      n1=n2;
      n2=n3;
    }
  }
}
```

## Go

```Go
package main

import "fmt"

func main(){
    var n int
    t1:=0
    t2:=1
    nextTerm:=0

    fmt.Print("Enter the number of terms : ")
    fmt.Scan(&n)
    fmt.Print("Fibonacci Series :")
    for i:=1;i<=n;i++ {
        if(i==1){
            fmt.Print(" ",t1)
            continue
        }
        if(i==2){
            fmt.Print(" ",t2)
            continue
        }
        nextTerm = t1 + t2
        t1=t2
        t2=nextTerm
        fmt.Print(" ",nextTerm)
    }
}
```

## Bash

```Bash
echo "How many number of terms to be generated ?"
read n
function fibonacci
{
  x=0
  y=1
  i=2
  echo "Fibonacci Series up to $n terms :"
  echo "$x"
  echo "$y"
  # -lt stands for equal to
  while [ $i -lt $n ]  
  do
      i=`expr $i + 1 `
      z=`expr $x + $y `
      echo "$z"
      x=$y
      y=$z
  done
}
r=`fibonacci $n`
echo "$r"
```

## JavaScript

```JavaScript
// program to generate fibonacci series up to n terms

// take input from the user
const number = parseInt(prompt('Enter the number of terms: '));
let n1 = 0, n2 = 1, nextTerm;

console.log('Fibonacci Series:');

for (let i = 1; i <= number; i++) {
    console.log(n1);
    nextTerm = n1 + n2;
    n1 = n2;
    n2 = nextTerm;
}
```
