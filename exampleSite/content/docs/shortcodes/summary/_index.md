---
title: Summary
customToC:
  - Title: Sample Text
  - Title: With Columns
    Headings:
      - Title: Sample
  - Title: Example usage
---

The **summary** shortcode extracts text from a source article.
Hugo maps content summaries to the `.Summary` page variable.
By default, this variable takes the first 70 words of content.

Adding a `<!--more-->` divider will override this automatic split
and give the content author full control over where the summary split will occur.
For more information on content summaries, check out the
[hugo documentation](https://gohugo.io/content-management/summaries/)

This works in much the same way as the `{{</* section "page" */>}}` shortcode,
however, this shortcode gives the content writer full control over how the page is structured.
Unlike the [section]({{< ref "docs/shortcodes/section/_index.md" >}}) shortcode,
the summary shortcode does not add the page title to the content.
This has the direct benefit preserving the native Table of Contents generation.

## [Sample Text]({{< relref "sample" >}})

{{< summary "sample" >}}

## With Columns

This shortcode should also work with columns:

{{< columns >}}

### Sample

{{< summary "sample" >}}

<--->

{{< summary "sample-2" true >}}

{{< /columns >}}

## Example usage

```tpl
## Sample page

{{</* summary "sample.md" */>}}
```
