---
title: Recipe
createdDate: 2023-11-12
---

The recipe shortcode generates a structured page from the detail within a recipe.yaml file stored in the data directory of the site.

This .yaml file should follow the follow pattern.

```yaml
title: Chopped Apples
description: This is a very simple recipe showcasing the layout of the recipe data file
source:
  title: Letter-Press
  author: MakeJames
  type: # string
  isbn: # string
  url: //:0
oven:
  temperature: # int
  units: # string
  fan: # bool
time:
  preperation: # time
  cooking: # time
yields:
  text: Serves
  quantity: 4
ingrediants:
  - name: apple (cox)
    amount: 4
    unit: # string
  - name: apple (russet)
    amount: 2
steps:
  - step: Chop Apples
    haccp:
      control_point: The apples must be clean
    notes:
      - Be careful with sharp objects
  - step: Share chopped apples across 4 plates
    notes:
      - Some people like green apples
      - Some people like red apples
notes:
  - This is a note
  - This is another note
```

{{< recipe "exampleRecipe" >}}
