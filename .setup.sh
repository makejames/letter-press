#!/bin/sh
EXAMPLE_SITE="exampleSite/"
THEME_DIR="exampleSite/themes/letter-press/"

mkdir -p $THEME_DIR
cp -r `ls -A | grep -v "exampleSite\|.git"` $THEME_DIR
