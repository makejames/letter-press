# Hugo Letter Press Theme

This theme is a fork of the [hugo-book](https://github.com/alex-shpak/hugo-book)
theme by [alex-shpak](https://github.com/alex-shpak).

This theme adds several of my own shortcodes and assets into a theme
and addresses some html styling issues with the original theme.

The initial improvements on the base repository are:

- All generated html is correctly formatted
- html styling and linting checks provided by html-validate pass
- outstanding accessibility errors are resolved

### [Hugo](https://gohugo.io) documentation theme as simple as plain book

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Menu](#menu)
- [Blog](#blog)
- [Configuration](#configuration)
- [Shortcodes](#shortcodes)
- [Versioning](#versioning)
- [Contributing](#contributing)

## Features

- Clean simple design
- Light and Mobile-Friendly
- Multi-language support
- Customisable
- Zero initial configuration
- Handy shortcodes
- Comments support
- Simple blog and taxonomy
- Primary features work without JavaScript
- Dark Mode

## Requirements

- Hugo 0.111 or higher
- Hugo extended version, read more [here](https://gohugo.io/news/0.48-relnotes/)

## Installation

### Install as git submodule

Navigate to your hugo project root and run:

```
git submodule add https://gitlab.com/makejames/letter-press themes/letter-press
```

Then run hugo (or set `theme = "letter-press"`/`theme: letter-press` in configuration file)

```
hugo server --minify --theme letter-press
```

### Install as hugo module

You can also add this theme as a Hugo module instead of a git submodule.

Start with initialising hugo modules, if not done yet:

```
hugo mod init gitlab.com/{repo/path}
```

Navigate to your hugo project root and add [module] section to your `config.toml`:

```toml
[module]
[[module.imports]]
path = 'gitlab.com/makejames/letter-press'
```

Then, to load/update the theme module and run hugo:

```sh
hugo mod get -u
hugo server
```

### Creating site from scratch

Below is an example on how to create a new site from scratch:

```sh
hugo new site mydocs; cd mydocs
git init
git submodule add https://gitlab.com/makejames/letter-press themes/letter-press
```

```sh
hugo server --minify --theme letter-press
```

## Code Styling

Hugo manages Code Syntax Highlighting as part of site generation.
It is possible to specify one of many available custom Syntax Highlighting options by viewing
[this Chroma Style Gallery](https://xyproto.github.io/splash/docs/all.html).
I recommend using `doom-one` as an accessible style.

```
[markup]
[markup.highlight]
  codeFences = true
  noClasses = true
  style = 'doom-one'
```

## Menu

### File tree menu (default)

By default, the theme will render pages from the `content/docs` section as a menu in a tree structure.

You can set `title` and `weight` in the front matter of pages to adjust the order and titles in the menu.

## Blog

A simple blog is supported in the section `posts`.

A blog is not the primary use-case of this theme, so it has only minimal features.

## Configuration

### Site Configuration

There are a few configuration options that you can add to your `config.toml` file.

You can also see the `[yaml` example](./exampleSite/config.yaml).

```toml
# (Optional) Set Google Analytics if you use it to track your website.
# Always put it on the top of the configuration file, otherwise it won't work
googleAnalytics = "UA-XXXXXXXXX-X"

# (Optional) If you provide a Disqus shortname, comments will be enabled on
# all pages.
disqusShortname = "my-site"

# (Optional) Set this to true if you use capital letters in file names
disablePathToLower = true

# (Optional) Set this to true to enable 'Last Modified by' date and git author
#  information on 'doc' type pages.
enableGitInfo = true

# (Optional) Theme is intended for documentation use, therefore it doesn't render taxonomy.
# You can remove related files with config below
disableKinds = ['taxonomy', 'taxonomyTerm']

[params]
  # (Optional, default light) Sets color theme: light, dark or auto.
  # Theme 'auto' switches between dark and light modes based on browser/os preferences
  BookTheme = 'light'

  # (Optional, default true) Controls table of contents visibility on right side of pages.
  # Start and end levels can be controlled with markup.tableOfContents setting.
  # You can also specify this parameter per page in front matter.
  BookToC = true

  # (Optional, default svg/logo.svg) Set the path to a logo for the book. If the logo is
  # /static/logo.svg then the path would be 'logo.svg'
  Logo = 'logo.svg'

  # (Optional, default none) Set the colour of the logo
  LogoColour = "#99270B"

  # (Optional, default false) To hide the logo set to true
  LogoHidden = false

  # (Optional, default none) Set leaf bundle to render as side menu
  # When not specified file structure and weights will be used
  # Deprecated, to be removed in June 2022
  BookMenuBundle = '/menu'

  # (Optional, default docs) Specify section of content to render as menu
  # You can also set value to "*" to render all sections to menu
  BookSection = 'docs'

  # Set source repository location.
  # Used for 'Last Modified' and 'Edit this page' links.
  BookRepo = 'https://gitlab.com/makejames/letter-press'

  # Specifies commit portion of the link to the page's last modified commit hash for 'doc' page
  # type.
  # Required if 'BookRepo' param is set.
  # Value used to construct a URL consisting of BookRepo/BookCommitPath/<commit-hash>
  # Github uses 'commit', Bitbucket uses 'commits'
  BookCommitPath = 'commit'

  # Enable 'Edit this page' links for 'doc' page type.
  # Disabled by default. Uncomment to enable. Requires 'BookRepo' param.
  # Path must point to the site directory.
  BookEditPath = 'edit/main/exampleSite'

  # (Optional, default January 2, 2006) Configure the date format used on the pages
  # - In git information
  # - In blog posts
  BookDateFormat = '2 Jan 2006'

  # (Optional, default true) Enables search function with flexsearch,
  # Index is built on fly, therefore it might slowdown your website.
  # Configuration for indexing can be adjusted in i18n folder per language.
  BookSearch = true

  # (Optional, default true) Enables comments template on pages
  # By default partials/docs/comments.html includes Disqus template
  # See https://gohugo.io/content-management/comments/#configure-disqus
  # Can be overwritten by same param in page frontmatter
  BookComments = true

  # /!\ This is an experimental feature, might be removed or changed at any time
  # (Optional, experimental, default false) Enables portable links and link checks in markdown pages.
  # Portable links meant to work with text editors and let you write markdown without {{< relref >}} shortcode
  # Theme will print warning if page referenced in markdown does not exists.
  BookPortableLinks = true

  # /!\ This is an experimental feature, might be removed or changed at any time
  # (Optional, experimental, default false) Enables service worker that caches visited pages and resources for offline use.
  BookServiceWorker = true
```

### Multi-Language Support

Theme supports Hugo's
[multilingual mode](https://gohugo.io/content-management/multilingual/),
just follow configuration guide there.
You can also tweak search indexing configuration per language in `i18n` folder.

### Page Configuration

You can specify additional params in the front matter of individual pages:

```toml
# Set type to 'docs' if you want to render page outside of configured section or if you render section other than 'docs'
type = 'docs'

# Set page weight to re-arrange items in file-tree menu (if BookMenuBundle not set)
weight = 10

# (Optional) Set to 'true' to mark page as flat section in file-tree menu (if BookMenuBundle not set)
bookFlatSection = false

# (Optional) Set to hide nested sections or pages at that level. Works only with file-tree menu mode
bookCollapseSection = true

# (Optional) Set true to hide page or section from side menu (if BookMenuBundle not set)
bookHidden = false

# (Optional) Set 'false' to hide ToC from page
bookToC = true

# (Optional) If you have enabled BookComments for the site, you can disable it for specific pages.
bookComments = true

# (Optional) Set to 'false' to exclude page from search index.
bookSearchExclude = true

# (Optional) Set explicit href attribute for this page in a menu (if BookMenuBundle not set)
bookHref = ''
```

### Partials

There are layout partials available for you to easily override components of the theme in `layouts/partials/`.

In addition to this,
there are several empty partials you can override to easily add/inject code.

| Empty Partial                                      | Placement                                   |
| -------------------------------------------------- | ------------------------------------------- |
| `layouts/partials/docs/inject/head.html`           | Before closing `<head>` tag                 |
| `layouts/partials/docs/inject/body.html`           | Before closing `<body>` tag                 |
| `layouts/partials/docs/inject/footer.html`         | After page footer content                   |
| `layouts/partials/docs/inject/menu-before.html`    | At the beginning of `<nav>` menu block      |
| `layouts/partials/docs/inject/menu-after.html`     | At the end of `<nav>` menu block            |
| `layouts/partials/docs/inject/content-before.html` | Before page content                         |
| `layouts/partials/docs/inject/content-after.html`  | After page content                          |
| `layouts/partials/docs/inject/toc-before.html`     | At the beginning of table of contents block |
| `layouts/partials/docs/inject/toc-after.html`      | At the end of table of contents block       |

### Extra Customisation

| File                     | Description                                                                           |
| ------------------------ | ------------------------------------------------------------------------------------- |
| `static/favicon.png`     | Override default favicon                                                              |
| `assets/_custom.scss`    | Customise or override scss styles                                                     |
| `assets/_variables.scss` | Override default SCSS variables                                                       |
| `assets/_fonts.scss`     | Replace default font with custom fonts (e.g. local files or remote like google fonts) |

### Plugins

There are a few features implemented as plugable `scss` styles.
Usually these are features that don't make it to the core but can still be useful.

| Plugin                            | Description                                                 |
| --------------------------------- | ----------------------------------------------------------- |
| `assets/plugins/_numbered.scss`   | Makes headings in markdown numbered, e.g. `1.1`, `1.2`      |
| `assets/plugins/_scrollbars.scss` | Overrides scrollbar styles to look similar across platforms |

To enable plugins,
add `@import "plugins/{name}";` to `assets/_custom.scss` in your website root.

### Hugo Internal Templates

There are a few hugo templates inserted in `<head>`

- [Google Analytics](https://gohugo.io/templates/internal/#google-analytics)
- [Open Graph](https://gohugo.io/templates/internal/#open-graph)

To disable Open Graph inclusion you can create your own empty file `\layouts\_internal\opengraph.html`.
In fact almost empty not quite empty because an empty file looks like absent for HUGO.
For example:

```
<!-- -->
```

## Shortcodes

- [Buttons](https://letter-press.makejames.com/docs/shortcodes/buttons/)
- [Columns](https://letter-press.makejames.com/docs/shortcodes/columns/)
- [Details](https://letter-press.makejames.com/docs/shortcodes/details/)
- [Hints](https://letter-press.makejames.com/docs/shortcodes/hints/)
- [Section](https://letter-press.makejames.com/docs/shortcodes/section/)
- [Summary](https://letter-press.makejames.com/docs/shortcodes/summary/)
- [Tabs](https://letter-press.makejames.com/docs/shortcodes/tabs/)

By default, Goldmark trims unsafe outputs which might prevent some shortcodes from rendering.
It is recommended to set `markup.goldmark.renderer.unsafe=true` if you encounter problems.

```toml
[markup.goldmark.renderer]
  unsafe = true
```

If you are using `config.yaml` or `config.json`, consult the
[configuration markup](https://gohugo.io/getting-started/configuration-markup/)

## Versioning

This theme follows a simple versioning of `MAJOR.MINOR`

- `MAJOR` releases indicate breaking changes from the previous `MAJOR` release
- `MINOR` releases are incremented on merges to `main` where there are no breaking
changes
- versions pre `1.0` are subject to breaking changes between `MINOR` versions.

If you want lower maintenance, use one of the released versions. If you want to
live on the bleeding edge of changes, you can use the `main` branch and update
your website when needed.

## Contributing

Contributors to the [original repository](https://github.com/alex-shpak/hugo-book/graphs/contributors)

Contributions are welcome and I will review and consider Merge Requests.
Primary goals are:

- Keep it simple.
- Keep minimal (or zero) default configuration.
- Avoid interference with user-defined layouts.
- Avoid using JS if it can be solved by CSS.
